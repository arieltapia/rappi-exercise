import { createSelector } from 'reselect';
import _ from 'lodash';

const categories = state => state.categories;
const products = state => state.products;
export const categorySelected = state => state.categorySelected;
const filterSelected = state => state.filterSelected;
export const search = state => state.search;


export const categoriesSelector = createSelector(
  [categories],
  (cat) => {
    cat.forEach((item) => {
      item.show = false;
    });
    return cat;
  },
);

const filterFunction = (list, filter) => {
  let newList = list.forEach((product) => {
    product.priceValue = parseInt(product.price.replace(/[^\d.]/g, ''), 10);
  });
  newList = filter.available ? list.filter(item => item.available) : list;
  switch (filter.value) {
    case 'lower_price':
      return _.orderBy(newList, 'priceValue', 'asc');
    case 'higher_price':
      return _.orderBy(newList, 'priceValue', 'desc');
    case 'higher_stock':
      return _.orderBy(newList, 'quantity', 'desc');
    default: return newList;
  }
};

const filterList = (productsList, selected, filter, searchVal) => {
  if (searchVal) {
    const searchList = [];
    productsList.forEach((i) => {
      if (_.includes(i.name, searchVal)) {
        if (filter.available === i.available) {
          searchList.push(i);
        }
      }
    });
    return searchList;
  }
  const filteredList = productsList.filter(item => item.sublevel_id === selected.id);
  filteredList.categoryName = selected.name;
  return filterFunction(filteredList, filter);
};

export const productsSelector = createSelector(
  products,
  categorySelected,
  filterSelected,
  search,
  filterList,
);
