import {
  GET_CATEGORIES, GET_PRODUCTS, CATEGORY_SELECTED, FILTER_SELECTED, SEARCH,
} from '../actionTypes';

export const categories = (state = [], action) => {
  switch (action.type) {
    case GET_CATEGORIES:
      return action.payload.categories;
    default: return state;
  }
};

export const products = (state = [], action) => {
  switch (action.type) {
    case GET_PRODUCTS:
      return [...action.payload.products];
    default: return state;
  }
};

export const categorySelected = (state = { id: null, name: null }, action) => {
  switch (action.type) {
    case CATEGORY_SELECTED:
      return action.payload;
    default: return state;
  }
};

export const filterSelected = (state = { value: null, available: true }, action) => {
  switch (action.type) {
    case FILTER_SELECTED:
      return action.payload;
    default: return state;
  }
};
export const search = (state = null, action) => {
  switch (action.type) {
    case SEARCH:
      return action.payload;
    default: return state;
  }
};
