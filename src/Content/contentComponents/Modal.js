import React from 'react';
import './modal.scss';

const Modal = props => (
  <div className="modal-container">
    <div className="modal-body">
      <div className="buy-dialog">
        <div className="buy-box">
          <button type="button" onClick={props.cancel} className="dismiss">x</button>
          <p> Agregar {props.item.name} al carrito ? </p>
          <p>Precio: ${props.item.price}</p>
          <div className="answer-options">
            <button type="button" onClick={props.accept} className="accept">Si</button>
            <button type="button" onClick={props.cancel} className="cancel">No</button>
          </div>
        </div>
      </div>
    </div>
  </div>
);


export default Modal;
