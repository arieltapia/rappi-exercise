import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import './sidebar.scss';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { categorySelected, searchSelection } from '../contentAction';


class SideBar extends Component {
  constructor(props) {
    super(props);
    this.state = {
      list: [],
    };
    this.toggleItem = this.toggleItem.bind(this);
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.list !== this.props.list) {
      this.setState({
        list: nextProps.list,
      });
    }
  }


  getCategoriesList() {
    return this.state.list.map(item => (
      <div key={item.id}>
        <li className="item">
          {item.name}
          <button type="button" onClick={() => { this.toggleItem(item.id); }}><FontAwesomeIcon icon="caret-down" /> </button>
        </li>
        {item.show
          && (
          <ul className="sublevel">
            {item.sublevels.map(subitem => (
              <li key={subitem.id + Date()}>
                <button
                  type="button"
                  onClick={() => { this.chooseCategory(subitem.id, subitem.name); }}
                >
                  {subitem.name}
                </button>
              </li>
            ))
              }
          </ul>
          )
        }
      </div>
    ));
  }

  chooseCategory(id, name) {
    this.props.searchSelection('');
    this.props.categorySelected({ id, name });
  }

  toggleItem(id) {
    const newList = this.state.list;
    newList.forEach((item) => {
      if (item.show === true) {
        item.show = false;
        return;
      }
      item.show = item.id === id;
    });
    this.setState({
      list: newList,
    });
  }

  render() {
    return (
      <div className="side-bar">
        <div className="title"> CATEGORIAS </div>
        <div>
          <ul>
            {this.getCategoriesList()}
          </ul>
        </div>
      </div>
    );
  }
}

const mapDispatchToProps = dispatch => bindActionCreators(
  {
    categorySelected,
    searchSelection,
  }, dispatch,
);

export default connect(null, mapDispatchToProps)(SideBar);
