import React from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import './products.scss';

const Product = (props) => {
  const getProducts = () => {
    if (!props.productList.length) {
      return (
        <div className="null-state">
          Elija una categoría o use el buscador para encontrar su producto.
        </div>
      );
    }
    return (
      <div className="shop-list">
        <p className="category-name">{props.productList.categoryName}</p>
        {
          props.productList.map(item => (
            <div className="product" key={item.id}>
              <img src={require('../../img/canasta.jpg')} alt="canasta" />
              <p>{item.name}</p>
              <p>{item.price}
                {item.available
                  && <button type="button" onClick={() => { props.addToCart(item); }}><FontAwesomeIcon icon="shopping-cart" /></button>
                }
              </p>
              <p>{`Stock: ${item.quantity}`}</p>
              <p className="no-stock">{!item.available ? 'NO DISPONIBLE' : ''}</p>
            </div>
          ))
        }
      </div>
    );
  };

  return <div className="products">{getProducts()}</div>;
};

export default Product;
