import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { filterSelection } from '../contentAction';
import { categorySelected } from '../../selectors';
import './filter.scss';

class Filter extends Component {
  constructor(props) {
    super(props);
    this.state = {
      value: 0,
      available: true,
    };
    this.handleChange = this.handleChange.bind(this);
    this.handleInputChange = this.handleInputChange.bind(this);
  }

  handleChange(event) {
    this.setState({ value: event.target.value },
      () => {
        const { value, available } = this.state;
        this.props.filterSelection({ value, available });
      });
  }

  handleInputChange(event) {
    const target = event.target;
    const checkValue = target.type === 'checkbox' ? target.checked : target.value;
    const name = target.name;

    this.setState({ [name]: checkValue },
      () => {
        const { value, available } = this.state;
        this.props.filterSelection({ value, available });
      });  
  }

  render() {
    return (
      <div className="filter">
        <label>
          Solo Disponibles:
          <input
            name="available"
            type="checkbox"
            checked={this.state.available}
            onChange={this.handleInputChange}
          />
        </label>
        Filtrar por:
        <select disabled={!this.props.categorySelected.id} className={!this.props.categorySelected.id ? 'disabled' : ''  } value={this.state.value} onChange={this.handleChange}>
          <option value="0">Sin Filtrar</option>
          <option value="lower_price">Menor Precio</option>
          <option value="higher_price">Mayor Precio</option>
          <option value="higher_stock">Mayor Stock</option>
        </select>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  categorySelected: categorySelected(state),
});

const mapDispatchToProps = dispatch => bindActionCreators(
  {
    filterSelection,
  }, dispatch,
);

export default connect(mapStateToProps, mapDispatchToProps)(Filter);
