import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { getCategories, getProducts } from './contentAction';
import { categoriesSelector, productsSelector } from '../selectors';
import './content.scss';
import Filter from './contentComponents/Filter';
import SideBar from './contentComponents/SideBar';
import Products from './contentComponents/Products';
import Modal from './contentComponents/Modal';

class Main extends Component {
  constructor(props) {
    super(props);
    this.props.getCategories();
    this.props.getProducts();
    this.state = {
      modal: false,
    };
    this.handleAddToCart = this.handleAddToCart.bind(this);
    this.handleAccept = this.handleAccept.bind(this);
    this.handleCancel = this.handleCancel.bind(this);
  }

  handleAddToCart(item) {
    this.setState(
      {
        item: {
          id: item.id,
          name: item.name,
          quantity: 1,
          stock: item.quantity,
          price: item.priceValue,
          total: item.priceValue,
        },
        modal: true,
      },
    );
  }

  handleAccept() {
    if (localStorage.getItem('cartList')) {
      const list = JSON.parse(localStorage.getItem('cartList'));
      let exist = false;
      list.forEach((cartItem) => {
        if (cartItem.id === this.state.item.id) {
          cartItem.quantity += 1;
          cartItem.total += this.state.item.price;
          exist = true;
        }
      });
      if (!exist) list.push(this.state.item);
      localStorage.setItem('cartList', JSON.stringify(list));
      exist = false;
      this.props.updateCart(list);
    } else {
      localStorage.setItem('cartList', JSON.stringify([this.state.item]));
      this.props.updateCart([this.state.item]);
    }
    this.setState({ modal: false });
  }

  handleCancel() {
    this.setState({
      modal: false,
    });
  }

  render() {
    return (
      <div className="content">
        { this.state.modal
          && (
            <Modal
              item={this.state.item}
              accept={this.handleAccept}
              cancel={this.handleCancel}
            />
          )
        }
        <Filter />
        <hr />
        <div className="shop-box">
          <SideBar
            list={this.props.categories}
          />
          <Products
            productList={this.props.products}
            addToCart={this.handleAddToCart}
          />
        </div>
      </div>
    );
  }
}
const mapStateToProps = state => ({
  categories: categoriesSelector(state),
  products: productsSelector(state),
});

const mapDispatchToProps = dispatch => bindActionCreators(
  {
    getCategories,
    getProducts,
  }, dispatch,
);

export default connect(mapStateToProps, mapDispatchToProps)(Main);
