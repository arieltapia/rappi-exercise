import {
  GET_CATEGORIES,
  GET_PRODUCTS,
  CATEGORY_SELECTED,
  FILTER_SELECTED,
  SEARCH,
} from '../actionTypes';
import categories from '../mockData/categories';
import products from '../mockData/products';


export const getCategories = () => (dispatch) => {
  dispatch({
    type: GET_CATEGORIES,
    payload: categories,
  });
};

export const getProducts = () => (dispatch) => {
  dispatch({
    type: GET_PRODUCTS,
    payload: products,
  });
};

export const categorySelected = category => (dispatch) => {
  dispatch({
    type: CATEGORY_SELECTED,
    payload: category,
  });
};

export const filterSelection = filter => (dispatch) => {
  dispatch({
    type: FILTER_SELECTED,
    payload: filter,
  });
};

export const searchSelection = search => (dispatch) => {
  dispatch({
    type: SEARCH,
    payload: search,
  });
};

