import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import './header.scss';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { searchSelection } from '../Content/contentAction';
import { search } from '../selectors';
import Cart from './HeaderComponents/Cart';

class Header extends Component {
  constructor(props) {
    super(props);
    this.state = {
      search: '',
      displayCart: false,
    };
    this.handleInput = this.handleInput.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleHover = this.handleHover.bind(this);
    this.handleLeave = this.handleLeave.bind(this);
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.searchValue !== this.props.searchValue) {
      this.setState({ search: nextProps.searchValue });
    }
  }

  handleInput(event) {
    this.setState({
      search: event.target.value,
    });
  }

  handleSubmit(event) {
    event.preventDefault();
    if (this.state.search) {
      this.props.searchSelection(this.state.search);
    }
  }

  handleHover() {
    this.setState({
      displayCart: true,
    });
  }

  handleLeave() {
    this.setState({
      displayCart: false,
    });
  }

  render() {
    const {
      cartCount, cartItems, addItem, removeItem, cartTotal, endBuy, thankMessage, removeWholeItem,
    } = this.props;
    return (
      <div className="container">
        <div className="top-title">
          <div className="title"> <h2>El Baratón</h2></div>
          <div className="search">
            <form onSubmit={this.handleSubmit}>
              <input type="text" value={this.state.search} onChange={this.handleInput} />
              <button type="submit"><FontAwesomeIcon icon="search" /></button>
            </form>
          </div>
          <div
            className="shopping"
            onFocus={this.handleHover}
            onMouseOver={this.handleHover}
            onMouseLeave={this.handleLeave}
          >
            <span className="cart-count">{cartCount}</span>
            <FontAwesomeIcon icon="shopping-bag" size="2x" />
            {this.state.displayCart
              && (
              <Cart
                cartItems={cartItems}
                addItem={addItem}
                removeItem={removeItem}
                removeWholeItem={removeWholeItem}
                cartTotal={cartTotal}
                endBuy={endBuy}
                thankMessage={thankMessage}
              />
              )
            }
          </div>
        </div>
        <div className="bottom-title"> Productos </div>
        <hr />
      </div>
    );
  }
}

const mapStateToProps = state => ({
  searchValue: search(state),
});

const mapDispatchToProps = dispatch => bindActionCreators(
  {
    searchSelection,
  }, dispatch,
);

export default connect(mapStateToProps, mapDispatchToProps)(Header);
