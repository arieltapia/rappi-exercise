import React from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import './cart.scss';

const Cart = props => (
  <div className="cart-container">
    <div className="cart-box">
      { !props.thankMessage && (
        props.cartItems.map(cartItem => (
          <div className="cart-item" key={cartItem.id}>
            <p>{cartItem.name}</p>
            <p className="add-remove">
              {`${cartItem.quantity} X $${cartItem.price}`}
              <button type="button" onClick={() => { props.addItem(cartItem); }}>+</button>
              <button type="button" onClick={() => { props.removeItem(cartItem.id); }}>-</button>
              <button type="button" onClick={() => { props.removeWholeItem(cartItem); }}><FontAwesomeIcon icon="trash-alt" /></button>
            </p>
          </div>
        ))
      )
      }
      { props.thankMessage && (
        <div className="thanks">Muchas gracias por su compra!</div>
      )}
      <div className="end-buy">
        <button type="button" onClick={props.endBuy} className="buy">Comprar</button>
        <span>Total: {`$${props.cartTotal}`}</span>
      </div>
    </div>
  </div>
);


export default Cart;
