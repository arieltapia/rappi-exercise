import React from 'react';
import { render } from 'react-dom';
import { Provider } from 'react-redux';
import { library } from '@fortawesome/fontawesome-svg-core';
import {
  faSearch, faShoppingBag, faCaretDown, faShoppingCart, faTrashAlt,
} from '@fortawesome/free-solid-svg-icons';
import store from './store';
import './index.css';
import App from './App';

library.add([faSearch, faShoppingBag, faCaretDown, faShoppingCart, faTrashAlt]);

render(
  <Provider store={store}>
    <App />
  </Provider>,
  document.getElementById('root'),
);
