import React, { Component } from 'react';
import './App.scss';
import _ from 'lodash';
import Header from './Header/Header';
import Content from './Content/Content';

class App extends Component {
  constructor(props) {
    super(props);
    let cartItems = [];
    let cartTotal = 0;
    if (localStorage.getItem('cartList')) {
      cartItems = JSON.parse(localStorage.getItem('cartList'));
      cartTotal = _.sumBy(cartItems, 'total');
    }
    this.state = {
      cartCount: cartItems.length ? cartItems.length : 0,
      cartItems,
      cartTotal,
    };
    this.handleUpdateCart = this.handleUpdateCart.bind(this);
    this.handleAddItem = this.handleAddItem.bind(this);
    this.handleRemoveItem = this.handleRemoveItem.bind(this);
    this.handleRemoveWholeItem = this.handleRemoveWholeItem.bind(this);
    this.handleEndBuy = this.handleEndBuy.bind(this);
  }

  handleUpdateCart(list) {
    this.setState({
      cartCount: list.length,
      cartItems: list,
      cartTotal: _.sumBy(list, 'total'),
    });
  }

  handleAddItem(item) {
    const cartItems = [...this.state.cartItems];
    let total = this.state.cartTotal;
    cartItems.forEach((product) => {
      if (product.id === item.id && product.quantity < item.stock) {
        product.quantity += 1;
        this.setState({ cartItems });
        total += product.total;
      }
    });
    this.setState({
      cartTotal: total,
    }, () => {
      localStorage.setItem('cartList', JSON.stringify(cartItems));
    });
  }

  handleRemoveItem(item) {
    const cartItems = [...this.state.cartItems];
    let total = this.state.cartTotal;
    cartItems.forEach((product) => {
      if (product.id === item && product.quantity > 1) {
        product.quantity -= 1;
        total -= product.total;
      }
    });
    this.setState({
      cartTotal: total,
    }, () => {
      localStorage.setItem('cartList', JSON.stringify(cartItems));
    });
  }

  handleRemoveWholeItem(item) {
    let cartItems = [...this.state.cartItems];
    cartItems = _.filter(cartItems, cartItem => cartItem.id !== item.id);
    const cartTotal = _.sumBy(cartItems, 'total');
    this.setState({
      cartItems,
      cartCount: cartItems.length ? cartItems.length : 0,
      cartTotal,
    }, () => {
      if (!cartItems.length) {
        localStorage.clear();
      } else {
        localStorage.setItem('cartList', JSON.stringify(cartItems));
      }
    });
  }

  handleEndBuy() {
    this.setState({
      cartTotal: 0,
      cartItems: [],
      cartCount: 0,
      thankMessage: true,
    }, () => {
      localStorage.clear();
      setTimeout(
        () => {
          this.setState({ thankMessage: false });
        }, 3000,
      );
    });
  }

  render() {
    const {
      cartCount, cartItems, cartTotal, thankMessage,
    } = this.state;
    return (
      <div className="App">
        <Header
          cartCount={cartCount}
          cartItems={cartItems}
          cartTotal={cartTotal}
          addItem={this.handleAddItem}
          removeItem={this.handleRemoveItem}
          removeWholeItem={this.handleRemoveWholeItem}
          endBuy={this.handleEndBuy}
          thankMessage={thankMessage}
        />
        <Content
          updateCart={this.handleUpdateCart}
        />
      </div>
    );
  }
}

export default App;
