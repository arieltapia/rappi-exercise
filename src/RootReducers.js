import { combineReducers } from 'redux';
import {
  categories, products, categorySelected, filterSelected, search,
} from './Content/contentReducer';


export default combineReducers({
  categories,
  products,
  categorySelected,
  filterSelected,
  search,
});
